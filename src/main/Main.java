import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> languages=new ArrayList<>();
        List<String> set=new ArrayList<>();
        set.add("sudoku");
        set.add("number");
        set.add("puzzle");
        languages.add("java");
        SortByNumberOfForks.setRepositories( RepoCrawler.getReposList(set,languages,10));
        new SortByNumberOfForks().sort();
    }
    }
